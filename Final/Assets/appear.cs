using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class appear : MonoBehaviour
{
    [SerializeField] private Transform vfx;
    private Rigidbody gun;

    // Update is called once per frame
    public void Awake()
    {
        gun = GetComponent<Rigidbody>();
        Instantiate(vfx, transform.position, Quaternion.identity);

    }

}
