using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BulletTarget : MonoBehaviour
{
    [SerializeField] public Text countText;
    //private BulletProjectile bulletprojectile;
    //private BulletProjectile bulletprojectile;
    public static int count;

    private void Start()
    {
        //target = GameObject.FindWithTag("target");
        SetCountText();
        
    }

    void Update()
    {
        count = BulletProjectile.score;
        SetCountText();
    }

    void SetCountText()
    {
        countText.text = "Score: " + count.ToString();
    }
}
