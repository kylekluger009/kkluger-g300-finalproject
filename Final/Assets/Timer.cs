using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Timer : MonoBehaviour
{

    public Text timerText;
    private float startTime;
    private bool finished = false;

    public GameObject winMessage;

    [SerializeField] public Text countText;


    

    // Start is called before the first frame update
    void Start()
    {
        startTime = Time.time;
        winMessage = GameObject.FindWithTag("done");
        winMessage.SetActive(false);  
    }

    // Update is called once per frame
    void Update()
    {
        if (finished)
        {
            return;
        }

        float t = Time.time - startTime;

        string minutes = ((int) t / 60).ToString();
        string seconds = (t % 60).ToString("f2");

        timerText.text = minutes + ":" + seconds;
    }


    //Got this function on unity answers https://answers.unity.com/questions/1226922/how-can-i-delay-for-3-seconds-an-scene-load.html
    private IEnumerator WaitForSceneLoad() 
    {
        yield return new WaitForSeconds(3);
        SceneManager.LoadScene(0);
     
    }



    public void Finished(){
        finished = true;
        timerText.color = Color.yellow;
        timerText.text = "Final Time: " + timerText.text ;

        countText.color = Color.yellow;
        countText.transform.position = new Vector3 (Screen.width * 0.5f, Screen.height * 0.9f, 0);

        countAndTime.lastScore = "Previous " + countText.text;

        countAndTime.lastTime = "Previous " + timerText.text;

        timerText.transform.position = new Vector3 (Screen.width * 0.5f, Screen.height * 0.75f, 0);
        winMessage.SetActive(true);

       



        StartCoroutine(WaitForSceneLoad());
    }


    
            
}
