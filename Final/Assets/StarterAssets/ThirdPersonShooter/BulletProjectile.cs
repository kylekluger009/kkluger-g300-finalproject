using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletProjectile : MonoBehaviour
{

  [SerializeField] private Transform vfxHitGreen;
  [SerializeField] private Transform vfxHitRed;
  private Rigidbody bulletRigidBody;

  public static int score = 0;

  private void Awake()
  {
      bulletRigidBody = GetComponent<Rigidbody>();
  }

  private void Start()
  {
      float speed = 50f;
      bulletRigidBody.velocity = transform.forward * speed;
  }


  private void OnTriggerEnter(Collider other)
  {
      if (other.GetComponent<isTarget>() != null)
      {
          Instantiate(vfxHitGreen, transform.position, Quaternion.identity);
          other.gameObject.SetActive (false);
          score += 1;

      }
          
      else
      {
          Instantiate(vfxHitRed, transform.position, Quaternion.identity);
      }
      Destroy(gameObject);
  }

}















